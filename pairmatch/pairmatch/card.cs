﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace pairmatch
{
    class card
    {

        //-------------------------------------
        // Type Definitions 
        //-------------------------------------
        public enum Symbol // symbols for the cards 
        {
            ZEBRA, //= 0
            SNAKE, // = 1 
            MOOSE, // = 2
            PANDA, // = 3
            BEAR, // = 4
                  //-------------------------------------
            NUM // = 5
        }


        //-------------------------------------
        // data
        //-------------------------------------

        Texture2D image = null;
        Texture2D cardSymbol = null;
        Vector2 postion;
        bool flipped = false;
        Symbol ourSymbol = Symbol.ZEBRA;
        //-------------------------------------
        // behaviour
        //-------------------------------------

        /*public card( Symbol newSymbol)
        {
            // constructor called when object is created
            // no returen type(special)
            // helps decide how object the will be set up
            // can have arguments (such as newScore)
            // This one lets us have access to game score 
            ourSymbol = newSymbol;
        }*/


            public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/card");
            cardSymbol = content.Load<Texture2D>("graphics/zebra");

            switch (ourSymbol)
            {
                case Symbol.ZEBRA:
                    cardSymbol = content.Load<Texture2D>("graphics/zebra");
                    break;
                case Symbol.SNAKE:
                    cardSymbol = content.Load<Texture2D>("graphics/snake");
                    break;
                case Symbol.MOOSE:
                    cardSymbol = content.Load<Texture2D>("graphics/moose");
                    break;
                case Symbol.PANDA:
                    cardSymbol = content.Load<Texture2D>("graphics/panda");
                    break;
                case Symbol.BEAR:
                    cardSymbol = content.Load<Texture2D>("graphics/bear");
                    break;
                default:
                    //this should never happen
                    break;
            }

        }



        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, postion, Color.White);
            if (flipped == true)
            {
                spriteBatch.Draw(cardSymbol, postion, Color.White);
            }

        }

        public void Input()
        {
            MouseState currentState = Mouse.GetState();
            Rectangle cardBounds = new Rectangle((int)postion.X, (int)postion.Y, image.Width, image.Height);
            if (currentState.LeftButton == ButtonState.Pressed && cardBounds.Contains(currentState.X, currentState.Y) && flipped == false)
            {
                flipped = true;
            }
        }

    }
}
